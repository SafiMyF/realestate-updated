﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for GroundPlan.xaml
    /// </summary>
    public partial class GroundPlan : UserControl
    {
        bool isScrollMoved = false;
        Button btnTouchedItem = null;

        public delegate void myViewOptions(object sender, EventArgs e);
        public event myViewOptions myEventOpenPlanView;

        public delegate void myLocationView(object sender, EventArgs e);
        public event myLocationView myEventLocView;

        public event EventHandler myEventVenDetailView;
        public event EventHandler myEventBackToVenture;
        public event EventHandler openFaq;

        public GroundPlan()
        {
            InitializeComponent();
            double ScrnHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double ScrnWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            grndPlanLayout.Height = ScrnHeight-70;
            grndPlanLayout.Width = ScrnWidth;

            Storyboard sb_Load = TryFindResource("VentureAnimi") as Storyboard;
            sb_Load.Begin();
        }
        
        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnPlanView_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;

        private void ScrollViewer_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    myEventOpenPlanView(btnTouchedItem, null);
                }
            }
        }

        private void ScrollViewer_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void ScrollViewer_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void ScrollViewer_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void btnLocation_TouchDown_1(object sender, TouchEventArgs e)
        {
            //Button btnLoc = sender as Button;
            myEventLocView(this, null);
        }

        //private void btnOpenDetails_TouchDown_1(object sender, TouchEventArgs e)
        //{
        //    myEventVenDetailView(this, null);
        //}
       
        private void btnBooking_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventVenDetailView(this, null);
        }

        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventBackToVenture(this, null);
        }

        public event EventHandler OpenGovtApproval;

        private void btnGovtApproval_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            OpenGovtApproval(this, null);
        }

        public event EventHandler OpenEncumberanceCert;
        private void btnEncCert_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            OpenEncumberanceCert(this, null);
        }

        private void btnFaq_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            openFaq(this, null);
        }
    }
}
