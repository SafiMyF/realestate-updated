﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for Location.xaml
    /// </summary>
    public partial class Location : UserControl
    {
        public Location()
        {
            InitializeComponent();
        }

        public event EventHandler CloseLocation;
        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            CloseLocation(this, null);
        }
    }
}
