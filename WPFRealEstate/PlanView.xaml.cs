﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for PlanView.xaml
    /// </summary>
    public partial class PlanView : UserControl
    {
        public PlanView()
        {
            InitializeComponent();
        }
        public Button PlanViewImg;
        public Image imgTemp;
        public Image imgTemp1;

        public event EventHandler ClosePlanView;

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (PlanViewImg != null)
            {
                grdImg.Children.Clear();
                imgTemp = new Image();
                imgTemp1 = PlanViewImg.Content as Image;
                imgTemp.Source = imgTemp1.Source;
                grdImg.Children.Add(imgTemp);
            }
        }

        private void btnClose_TouchDown_1(object sender, TouchEventArgs e)
        {
            //this.Visibility = Visibility.Collapsed;
            ClosePlanView(this, null);
        }

    }
}
