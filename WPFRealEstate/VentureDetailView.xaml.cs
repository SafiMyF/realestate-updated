﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for VentureDetailView.xaml
    /// </summary>
    public partial class VentureDetailView : UserControl
    {
        public VentureDetailView()
        {
            InitializeComponent();
        }

        public event EventHandler myEventBacktoGroundPlan;

        private void btnOpenDetails_TouchDown_1(object sender, TouchEventArgs e)
        {
            LoadBookingPanel();
        }

        private void LoadBookingPanel()
        {
            PlotDetails.Visibility = Visibility.Visible;
            BookingForm.Visibility = Visibility.Visible;
            brdBg.Visibility = Visibility.Visible;
            Storyboard PlotDetailsSB = this.TryFindResource("PlotDetails_SB") as Storyboard;
            PlotDetailsSB.Begin();           
        }

        private void txtName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
                txtName.Text = "Enter Name";
        }

        private void txtName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == "Enter Name")
                txtName.Text = "";
        }

        private void txtPhone_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPhone.Text))
                txtPhone.Text = "Enter Phone Number";
        }

        private void txtPhone_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtPhone.Text == "Enter Phone Number")
                txtPhone.Text = "";
        }

        private void btnBack_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventBacktoGroundPlan(this, null);
        }

        private void txtEmailId_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmailId.Text))
                txtEmailId.Text = "Enter Email Id";
        }

        private void txtEmailId_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtEmailId.Text == "Enter Email Id")
                txtEmailId.Text = "";
        }

        private void txtAdvPay_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtAdvPay.Text))
                txtAdvPay.Text = "Enter Advance Amount";
        }

        private void txtAdvPay_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtAdvPay.Text == "Enter Advance Amount")
                txtAdvPay.Text = "";
        }

        private void btnCancelBooking_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {

            Storyboard PlotDetailsSB = this.TryFindResource("PlotDetails_SB_Exit") as Storyboard;
            PlotDetailsSB.Begin();

            //PlotDetails.Visibility = Visibility.Collapsed;
            //BookingForm.Visibility = Visibility.Collapsed;
        }

        private void CboSelectPlot_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {           
            if (CboSelectPlot.SelectedIndex > 0 )
            {
                LoadBookingPanel();
            }           
        }
        
    }
}
