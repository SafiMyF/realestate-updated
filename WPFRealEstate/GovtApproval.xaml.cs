﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for GovtApproval.xaml
    /// </summary>
    public partial class GovtApproval : UserControl
    {
        public GovtApproval()
        {
            InitializeComponent();
            //this.Loaded += OnLoaded;          
        }
        
        //private const string hostUri = "http://localhost:8088/PsuedoWebHost/";
        //private HttpListener _httpListener;

        //private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        //{
        //    // Get the PDF from the 'database'
        //    byte[] pdfBytes = GetPdfData();

        //    // Create a PDF server that serves the PDF to a browser
        //    CreatePdfServer(pdfBytes);

        //    // Cleanup after the browser finishes navigating
        //    Browser.Navigated += BrowserOnNavigated;
        //    Browser.Navigate(hostUri);
        //}

        //private byte[] GetPdfData()
        //{
        //    // TODO: Replace this code with data access code
        //    // TODO: Pick a file from the file system for this demo.
        //    string path =  @"Files/GovtApproval.pdf";
        //    byte[] pdfBytes = File.ReadAllBytes(path);

        //    // Return the raw data
        //    return pdfBytes;
        //}

        //private void CreatePdfServer(byte[] pdfBytes)
        //{
        //    _httpListener = new HttpListener();
        //    _httpListener.Prefixes.Add(hostUri);
        //    _httpListener.Start();

        //    _httpListener.BeginGetContext((ar) =>
        //    {
        //        HttpListenerContext context = _httpListener.EndGetContext(ar);

        //        // Obtain a response object.
        //        HttpListenerResponse response = context.Response;
        //        response.StatusCode = (int)HttpStatusCode.OK;
        //        response.ContentType = "application/pdf";

        //        // Construct a response.
        //        if (pdfBytes != null)
        //        {
        //            response.ContentLength64 = pdfBytes.Length;

        //            // Get a response stream and write the PDF to it.
        //            Stream oStream = response.OutputStream;
        //            oStream.Write(pdfBytes, 0, pdfBytes.Length);
        //            oStream.Flush();
        //            oStream.Close();
        //        }

        //        response.Close();
        //        _httpListener.Close();
        //    }, null);


        //}

        ///// <summary>
        ///// Stops the http listener after the browser has finished loading the document
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="navigationEventArgs"></param>
        //private void BrowserOnNavigated(object sender, NavigationEventArgs navigationEventArgs)
        //{
            
        //    Browser.Navigated -= BrowserOnNavigated;
        //}

        public event EventHandler myEventClosePDF;
        private void btnClosePDF_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventClosePDF(this, null);
        }

        private void scrollViewerGOV_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
