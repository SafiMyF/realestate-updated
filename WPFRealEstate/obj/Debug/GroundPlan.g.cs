﻿#pragma checksum "..\..\GroundPlan.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EECB39B7C6E47A855BD7C21CD53C4E4B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace WPFRealEstate {
    
    
    /// <summary>
    /// GroundPlan
    /// </summary>
    public partial class GroundPlan : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 114 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grndPlanLayout;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdLayoutView;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView2;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView3;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView4;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView5;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView6;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView7;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView8;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView9;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView10;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView11;
        
        #line default
        #line hidden
        
        
        #line 249 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPlanView12;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBooking;
        
        #line default
        #line hidden
        
        
        #line 268 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMap;
        
        #line default
        #line hidden
        
        
        #line 273 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFaq;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGovtApproval;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEncCert;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 311 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img1;
        
        #line default
        #line hidden
        
        
        #line 321 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img2;
        
        #line default
        #line hidden
        
        
        #line 331 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img3;
        
        #line default
        #line hidden
        
        
        #line 341 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img4;
        
        #line default
        #line hidden
        
        
        #line 351 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img5;
        
        #line default
        #line hidden
        
        
        #line 361 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img6;
        
        #line default
        #line hidden
        
        
        #line 371 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image img7;
        
        #line default
        #line hidden
        
        
        #line 396 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border AboutLayout;
        
        #line default
        #line hidden
        
        
        #line 430 "..\..\GroundPlan.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBack;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RealEstate;component/groundplan.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\GroundPlan.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grndPlanLayout = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.grdLayoutView = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            
            #line 138 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            
            #line 139 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchUp_1);
            
            #line default
            #line hidden
            
            #line 140 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).ScrollChanged += new System.Windows.Controls.ScrollChangedEventHandler(this.ScrollViewer_ScrollChanged_1);
            
            #line default
            #line hidden
            
            #line 140 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).PreviewTouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchDown_1);
            
            #line default
            #line hidden
            
            #line 141 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchMove_1);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnPlanView = ((System.Windows.Controls.Button)(target));
            
            #line 149 "..\..\GroundPlan.xaml"
            this.btnPlanView.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnPlanView2 = ((System.Windows.Controls.Button)(target));
            
            #line 158 "..\..\GroundPlan.xaml"
            this.btnPlanView2.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnPlanView3 = ((System.Windows.Controls.Button)(target));
            
            #line 167 "..\..\GroundPlan.xaml"
            this.btnPlanView3.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnPlanView4 = ((System.Windows.Controls.Button)(target));
            
            #line 176 "..\..\GroundPlan.xaml"
            this.btnPlanView4.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnPlanView5 = ((System.Windows.Controls.Button)(target));
            
            #line 185 "..\..\GroundPlan.xaml"
            this.btnPlanView5.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnPlanView6 = ((System.Windows.Controls.Button)(target));
            
            #line 194 "..\..\GroundPlan.xaml"
            this.btnPlanView6.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnPlanView7 = ((System.Windows.Controls.Button)(target));
            
            #line 204 "..\..\GroundPlan.xaml"
            this.btnPlanView7.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnPlanView8 = ((System.Windows.Controls.Button)(target));
            
            #line 213 "..\..\GroundPlan.xaml"
            this.btnPlanView8.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnPlanView9 = ((System.Windows.Controls.Button)(target));
            
            #line 222 "..\..\GroundPlan.xaml"
            this.btnPlanView9.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btnPlanView10 = ((System.Windows.Controls.Button)(target));
            
            #line 231 "..\..\GroundPlan.xaml"
            this.btnPlanView10.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btnPlanView11 = ((System.Windows.Controls.Button)(target));
            
            #line 240 "..\..\GroundPlan.xaml"
            this.btnPlanView11.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnPlanView12 = ((System.Windows.Controls.Button)(target));
            
            #line 249 "..\..\GroundPlan.xaml"
            this.btnPlanView12.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlanView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnBooking = ((System.Windows.Controls.Button)(target));
            
            #line 262 "..\..\GroundPlan.xaml"
            this.btnBooking.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBooking_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnMap = ((System.Windows.Controls.Button)(target));
            
            #line 268 "..\..\GroundPlan.xaml"
            this.btnMap.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnLocation_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnFaq = ((System.Windows.Controls.Button)(target));
            
            #line 273 "..\..\GroundPlan.xaml"
            this.btnFaq.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnFaq_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btnGovtApproval = ((System.Windows.Controls.Button)(target));
            
            #line 278 "..\..\GroundPlan.xaml"
            this.btnGovtApproval.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnGovtApproval_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btnEncCert = ((System.Windows.Controls.Button)(target));
            
            #line 284 "..\..\GroundPlan.xaml"
            this.btnEncCert.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnEncCert_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 21:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 22:
            this.img1 = ((System.Windows.Controls.Image)(target));
            return;
            case 23:
            this.img2 = ((System.Windows.Controls.Image)(target));
            return;
            case 24:
            this.img3 = ((System.Windows.Controls.Image)(target));
            return;
            case 25:
            this.img4 = ((System.Windows.Controls.Image)(target));
            return;
            case 26:
            this.img5 = ((System.Windows.Controls.Image)(target));
            return;
            case 27:
            this.img6 = ((System.Windows.Controls.Image)(target));
            return;
            case 28:
            this.img7 = ((System.Windows.Controls.Image)(target));
            return;
            case 29:
            this.AboutLayout = ((System.Windows.Controls.Border)(target));
            return;
            case 30:
            
            #line 398 "..\..\GroundPlan.xaml"
            ((System.Windows.Controls.ScrollViewer)(target)).ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 31:
            this.btnBack = ((System.Windows.Controls.Button)(target));
            
            #line 430 "..\..\GroundPlan.xaml"
            this.btnBack.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBack_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

