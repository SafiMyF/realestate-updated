﻿#pragma checksum "..\..\ViewOptions.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "067E8912B0846EAA35E9BD37AF36F649"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace WPFRealEstate {
    
    
    /// <summary>
    /// ViewOptions
    /// </summary>
    public partial class ViewOptions : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ViewOptsMainPanel;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLayoutView;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn3DView;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grd3DView;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutOptionsView;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAboutUs;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGroundPlan;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFloorPlan;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLocation;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOtherPlots;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\ViewOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TabChildrenPanel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WPFRealEstate;component/viewoptions.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ViewOptions.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\ViewOptions.xaml"
            ((WPFRealEstate.ViewOptions)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ViewOptsMainPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btnLayoutView = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\ViewOptions.xaml"
            this.btnLayoutView.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnLayoutView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btn3DView = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\ViewOptions.xaml"
            this.btn3DView.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btn3DView_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 5:
            this.grd3DView = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.LayoutOptionsView = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.btnAboutUs = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\ViewOptions.xaml"
            this.btnAboutUs.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnAboutUs_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnGroundPlan = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\ViewOptions.xaml"
            this.btnGroundPlan.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnGroundPlan_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnFloorPlan = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\ViewOptions.xaml"
            this.btnFloorPlan.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnFloorPlan_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnLocation = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\ViewOptions.xaml"
            this.btnLocation.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnLocation_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnOtherPlots = ((System.Windows.Controls.Button)(target));
            
            #line 84 "..\..\ViewOptions.xaml"
            this.btnOtherPlots.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnOtherPlots_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 12:
            this.TabChildrenPanel = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

