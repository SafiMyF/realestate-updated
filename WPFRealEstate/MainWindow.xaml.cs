﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFRealEstate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void btnVenture_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Stop();
            VenturesListAreaPanel.Visibility = Visibility.Collapsed;
            LayoutRoot.Visibility = Visibility.Collapsed;
        }
                      
        //Initialize i. this value reflects PlanView margin.
        public int i = 20;
    
        private void btnHome_TouchDown_1(object sender, TouchEventArgs e)
        {
            i = 100;
            btnVenturesList.Visibility = Visibility.Collapsed;
            //Finds the HomeSB animation in the MainWindow page and Begins the animation from begining.
            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Seek(TimeSpan.Zero);

            ParentPanel.Children.Clear();   //Clears the ParentPanel Content.
            VenturesListAreaPanel.Visibility = Visibility.Visible;  //ventureslist panel visible.
            LayoutRoot.Visibility = Visibility.Visible;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            //Finds the HomeSB animation in the MainWindow page and Begins the animation.
            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Begin();

            //Raise Events to manipulate objects.
            this.ManipulationStarting -= MainWindow_ManipulationStarting;
            this.ManipulationDelta -= MainWindow_ManipulationDelta;
            this.ManipulationStarting += MainWindow_ManipulationStarting;
            this.ManipulationDelta += MainWindow_ManipulationDelta;
        }

        ManipulationModes currentMode = ManipulationModes.All;
        void MainWindow_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            var element = args.OriginalSource as UIElement;

            var transformation = element.RenderTransform
                                                 as MatrixTransform;
            var matrix = transformation == null ? Matrix.Identity :
                                           transformation.Matrix;

            matrix.ScaleAt(args.DeltaManipulation.Scale.X,
                           args.DeltaManipulation.Scale.Y,
                           args.ManipulationOrigin.X,
                           args.ManipulationOrigin.Y);

            matrix.RotateAt(args.DeltaManipulation.Rotation,
                            args.ManipulationOrigin.X,
                            args.ManipulationOrigin.Y);

            matrix.Translate(args.DeltaManipulation.Translation.X,
                             args.DeltaManipulation.Translation.Y);

            element.RenderTransform = new MatrixTransform(matrix);
            args.Handled = true;           
        }

        void MainWindow_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;
            
            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);

            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }

        private void btnContacUs_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadContactUs();
        }
        private void LoadContactUs()
        {
            //ContactUs.Visibility = Visibility.Visible;
            ContactUs Cnt = new ContactUs();
            if (checkContactUs())           //check ContactUs Exists in parentPanel or not.
            {

                Cnt.RenderTransform = new MatrixTransform(1, 0, 0, 1, 0, 0);
                Cnt.BringIntoView();
                Panel.SetZIndex(Cnt, 9999);
                ParentPanel.Children.Add(Cnt);
                //Raise Contact Us Events.
                Cnt.myEventCloseContactUs += Cnt_myEventCloseContactUs;
            }
        }

        private bool checkContactUs()
        {
            foreach (UserControl item in ParentPanel.Children)
            {
                if (item is ContactUs)
                    return false;       //ContactUs Exists in parentPanel
            }
            return true;                //ContactUs Doesn't Exists in parentPanel.
        }

        void Cnt_myEventCloseContactUs(object sender, EventArgs e)
        {
            ContactUs Cnt = (ContactUs)sender;
            ParentPanel.Children.Remove(Cnt);
        }

        private void btnVenturesList_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadVentures();
        }

        void ventrs_myEventOpenSelectedVenture(object sender, EventArgs e)
        {
            LoadGroundPlan();
        }

        
        private void LoadGroundPlan()
        {
            //Load the layout of selected venture in groundplan.
            btnVenturesList.Visibility = Visibility.Visible;
            GroundPlan GPlan = new GroundPlan();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(GPlan);
            //Raise Events from GroundPlan.
            GPlan.myEventOpenPlanView += GPlan_myEventOpenPlanView;
            GPlan.myEventLocView += GPlan_myEventLocView;
            GPlan.myEventBackToVenture += GPlan_myEventBackToVenture;
            GPlan.myEventVenDetailView += GPlan_myEventVenDetailView;
            GPlan.OpenGovtApproval += GPlan_OpenGovtApproval;
            GPlan.OpenEncumberanceCert += GPlan_OpenEncumberanceCert;
            GPlan.openFaq += GPlan_openFaq;
        }

        void GPlan_openFaq(object sender, EventArgs e)
        {
            FAQ faq = new FAQ();
            ParentPanel.Children.Add(faq);
            //Raise Events from FAQ
            faq.eventCloseFaq += faq_eventCloseFaq;
        }

        void faq_eventCloseFaq(object sender, EventArgs e)
        {
            FAQ faq = (FAQ)sender;
            ParentPanel.Children.Remove(faq);
        }

        void GPlan_OpenEncumberanceCert(object sender, EventArgs e)
        {
            EncumberanceCert EC = new EncumberanceCert();
            EC.BringIntoView();
            Panel.SetZIndex(EC, 9999);
            ParentPanel.Children.Add(EC);
            EC.myEventClosePDF += EC_myEventClosePDF;
        }

        void EC_myEventClosePDF(object sender, EventArgs e)
        {
            EncumberanceCert EC = (EncumberanceCert)sender;
            ParentPanel.Children.Remove(EC);
        }

        void GPlan_OpenGovtApproval(object sender, EventArgs e)
        {
            GovA = new GovtApproval();
            GovA.BringIntoView();
            Panel.SetZIndex(GovA, 9999);
            ParentPanel.Children.Add(GovA);
            GovA.myEventClosePDF += GovA_myEventClosePDF;
        }
        GovtApproval GovA = new GovtApproval();
        void GovA_myEventClosePDF(object sender, EventArgs e)
        {
            GovA = (GovtApproval)sender;
            ParentPanel.Children.Remove(GovA);
        }
                
        void GPlan_myEventOpenPlanView(object sender, EventArgs e)
        {
            //Open PlanView from GroundPlan.
            if (i == 100)
                i = 10;

            PlanView PVw = new PlanView();
            Button btnDets = sender as Button;
            
            if (btnDets != null)
            {
                PVw.PlanViewImg = btnDets;
                PVw.IsManipulationEnabled = true;
                PVw.RenderTransform = new MatrixTransform(1, 0, 0, 1, i + 20, i + 20);
                PVw.BringIntoView();

                Panel.SetZIndex(PVw, 9999);
                ParentPanel.Children.Add(PVw);
                i = i + 10;
            }
            PVw.ClosePlanView += PVw_ClosePlanView;
        }

        void PVw_ClosePlanView(object sender, EventArgs e)
        {
            PlanView PVw = (PlanView)sender;
            ParentPanel.Children.Remove(PVw);
        }
        
        void GPlan_myEventLocView(object sender, EventArgs e)
        {
            Location Loc = new Location();
            if (CheckLoc())
            {                
                ParentPanel.Children.Add(Loc);
            }
            //Raise events from Location
            Loc.CloseLocation += Loc_CloseLocation;
        }

        void Loc_CloseLocation(object sender, EventArgs e)
        {
            Location Loc = (Location)sender;
            ParentPanel.Children.Remove(Loc);
        }

        private bool CheckLoc()
        {
            foreach (UserControl item in ParentPanel.Children)
            {
                if (item is Location)
                    return false;       //Location Exists in parentPanel
            }
            return true;                //Location Doesn't Exists in parentPanel.
        }

        void GPlan_myEventBackToVenture(object sender, EventArgs e)
        {
            LoadVentures();
        }
        
        private void btnOpenVenture_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadVentures();
        }

        private void LoadVentures()
        {
            //Home animation starts here
            Storyboard HomeSB = this.TryFindResource("HomeAnimi") as Storyboard;
            HomeSB.Stop();
            //Home animation ends here
            btnVenturesList.Visibility = Visibility.Collapsed;
            VenturesListAreaPanel.Visibility = Visibility.Collapsed;    //Collapse Venture list area
            LayoutRoot.Visibility = Visibility.Collapsed;

            //Create Venture object, Clear all Childrens in ParentPanel and add Ventures.
            Ventures ventrs = new Ventures();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(ventrs);

            //Raise Events from Ventures.
            ventrs.myEventOpenSelectedVenture += ventrs_myEventOpenSelectedVenture; //Opens Selected Venture.
        }

        void GPlan_myEventVenDetailView(object sender, EventArgs e)
        {
            LoadVentureDetailView();
        }

        private void LoadVentureDetailView()
        {
            //Load VentureDetailView
            VentureDetailView VDV = new VentureDetailView();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(VDV);
            VDV.GroundLayout.IsManipulationEnabled = true;
            VDV.GroundLayout.RenderTransform = new MatrixTransform(1, 0, 0, 1, 10, 10);
            VDV.GroundLayout.BringIntoView();
            Panel.SetZIndex(VDV.GroundLayout, 9999);
            //Raise Events from VentureDetailView.
            VDV.myEventBacktoGroundPlan += VDV_myEventBacktoGroundPlan;
        }

        void VDV_myEventBacktoGroundPlan(object sender, EventArgs e)
        {
            //LoadGroundPlan
            LoadGroundPlan();
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnAboutus_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            Aboutus abt = new Aboutus();
            ParentPanel.Children.Add(abt);
            //Raise About us events
            abt.eventCloseAbout += abt_eventCloseAbout;
        }
        void abt_eventCloseAbout(object sender, EventArgs e)
        {
            Aboutus abt = (Aboutus)sender;
            ParentPanel.Children.Remove(abt);
        }
    }
}
